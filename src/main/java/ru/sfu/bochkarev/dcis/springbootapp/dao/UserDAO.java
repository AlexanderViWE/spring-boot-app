package ru.sfu.bochkarev.dcis.springbootapp.dao;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.sfu.bochkarev.dcis.springbootapp.models.User;

/**
 * Класс для доступа к данным аккаунтов. 
 * Данный класс нужен только для внесения новых пользователей в БД.
 * 
 * @author Sasha
 */
@Component
public class UserDAO {  
  // TODO: Придумать более хороший способ регистрации пользователей.
  //       Возможно spring security имеет реализацию добавления записей в бд.
  
  JdbcTemplate jdbcTemplate;

  @Autowired
  public void setDataSource(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }

  public User findByUsername(String username) {
    return jdbcTemplate.query("SELECT * FROM users WHERE username=?",
        new BeanPropertyRowMapper<>(User.class), username).stream().findAny().orElse(null);
  }

  public int create(User user) {
    return jdbcTemplate.update(
        "INSERT INTO users (username, password, role, enabled) VALUES (?,?,?,?)",
        user.getUsername(), user.getPassword(), "ROLE_" + user.getRole(), user.getEnabled());
  }
}
