package ru.sfu.bochkarev.dcis.springbootapp.dao;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.sfu.bochkarev.dcis.springbootapp.models.Smartphone;

/**
 * Доступ к данны БД о смартфонах
 */
@Component
public class SmartphoneDAO {
  JdbcTemplate jdbcTemplate;

  @Autowired
  public void setDataSource(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }

  /**
   * Получить список всех смартфонов
   */
  public List<Smartphone> findAll() {
    List<Smartphone> smartphones = jdbcTemplate.query("SELECT * FROM smartphone",
        new BeanPropertyRowMapper<>(Smartphone.class));
    return smartphones;
  }

  /**
   * Получить смартфонов по id
   */
  public Smartphone findById(int id) {
    return jdbcTemplate.query("SELECT * FROM smartphone WHERE id=?",
        new BeanPropertyRowMapper<>(Smartphone.class), id).stream().findAny().orElse(null);
  }

  /**
   * Добавить новый смартфон
   */
  public int insert(Smartphone smartphone) {
    Integer id = jdbcTemplate.queryForObject(
        "INSERT INTO smartphone (manufacturer, modelType, processorType, weight, releaseYear, enabled)"
            + "VALUES (?,?,?,?,?,?) RETURNING id",
            Integer.class, 
        smartphone.getManufacturer(), smartphone.getModelType(), smartphone.getProcessorType(),
        smartphone.getWeight(), smartphone.getReleaseYear(), smartphone.getEnabled());
    smartphone.setId(id);
    return id;
  }

  /**
   * Обновить данные смартфона по id
   */
  public int updateById(int id, Smartphone smartphone) {
    return jdbcTemplate.update("UPDATE smartphone SET "
        + "manufacturer=?, modelType=?, processorType=?, weight=?, releaseYear=?, enabled=?" + 
        " WHERE id=?",
        smartphone.getManufacturer(), smartphone.getModelType(), smartphone.getProcessorType(),
        smartphone.getWeight(), smartphone.getReleaseYear(), smartphone.getEnabled(), id);
  }

  /**
   * Удалить смартфон по id
   */
  public int removeById(int id) {
    return jdbcTemplate.update("DELETE FROM smartphone WHERE id=?", id);
  }

  /**
   * Получить список смартфонов год выпуска которых больше определенного значения
   */
  public List<Smartphone> findWhereReleaseYearMore(int releaseYear) {
    List<Smartphone> smartphones =
        jdbcTemplate.query("SELECT * FROM smartphone WHERE releaseyear>=?",
            new BeanPropertyRowMapper<>(Smartphone.class), releaseYear);
    return smartphones;
  }  
}
