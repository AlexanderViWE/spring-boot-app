package ru.sfu.bochkarev.dcis.springbootapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Контроллер для главной страницы
 */
@Controller
public class MainPageController {
  /**
   * Главная страница
   */
  @GetMapping("/")
  public String index(Model model) {
    return "index";
  }
}