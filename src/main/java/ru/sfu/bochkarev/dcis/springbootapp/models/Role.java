package ru.sfu.bochkarev.dcis.springbootapp.models;

public enum Role {
  USER,
  ADMIN;
}
