package ru.sfu.bochkarev.dcis.springbootapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import jakarta.validation.Valid;
import ru.sfu.bochkarev.dcis.springbootapp.dao.UserDAO;
import ru.sfu.bochkarev.dcis.springbootapp.models.Role;
import ru.sfu.bochkarev.dcis.springbootapp.models.User;

@Controller
@RequestMapping("/auth")
public class AuthController {
  @Autowired
  UserDAO userDAO;
  @Autowired
  PasswordEncoder passwordEncoder;

  @GetMapping("/login")
  public String getLoginPage(Model model,
      @RequestParam(value = "error", required = false) String error) {
    if (error != null)
      model.addAttribute("loginError", true);
    return "auth/login";
  }
  
//  @PostMapping("/login")
//  public String addUser(@Valid User userForm, BindingResult bindingResult, Model model) {
//
//      if (bindingResult.hasErrors()) {
//          return "registration";
//      }
//      if (!userForm.getPassword().equals(userForm.getPasswordConfirm())){
//          model.addAttribute("passwordError", "Пароли не совпадают");
//          return "registration";
//      }
//      if (!userService.saveUser(userForm)){
//          model.addAttribute("usernameError", "Пользователь с таким именем уже существует");
//          return "registration";
//      }
//
//      return "redirect:/";
//  }

  @GetMapping("/registration")
  public String getRegistrationPage(@ModelAttribute("user") User user) {
    return "auth/registration";
  }
  
  @PostMapping("/registration")
  public String creareUser(Model model, @ModelAttribute("user") @Valid User user,
      BindingResult bindingResult) {        
    if (userDAO.findByUsername(user.getUsername()) != null) {
      model.addAttribute("usernameExist", true);
      return "auth/registration";
    }
        
    if (bindingResult.hasErrors())
      return "auth/registration";
    
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    user.setRole(Role.USER.name());
    user.setEnabled(true);
    
    userDAO.create(user);
    return "redirect:/auth/login";
  }
}