package ru.sfu.bochkarev.dcis.springbootapp.config;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import ru.sfu.bochkarev.dcis.springbootapp.models.Role;

@Configuration
public class SecurityConfig{
  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
  
  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    http
    .csrf().disable()
    .authorizeHttpRequests((requests) -> requests
      .requestMatchers("/styles/**").permitAll()
      .requestMatchers("/", "/auth/**").permitAll()
      .requestMatchers("/smartphones", "/smartphones/filter/**", "/smartphones/buy").hasAnyRole(Role.USER.name(), Role.ADMIN.name())
      .requestMatchers("/smartphones/**").hasRole(Role.ADMIN.name())
      .requestMatchers("/**").permitAll()
    )
    .formLogin((form) -> form
      .loginPage("/auth/login")
      .loginProcessingUrl("/auth/login")
      .defaultSuccessUrl("/")
      .permitAll()
    )
    .logout((logout) -> logout
      .logoutUrl("/auth/logout")
      .logoutSuccessUrl("/")
      .permitAll()
    );

    return http.build();
  }
    
//  @Bean
//  public UserDetailsService userDetailsService() {
//    Collection<UserDetails> users = new ArrayList<>();
//    users.add(User.builder().username("user").password(passwordEncoder().encode("user"))
//        .roles(Role.USER.name()).build());
//
//    users.add(User.builder().username("admin").password(passwordEncoder().encode("admin"))
//        .roles(Role.ADMIN.name()).build());
//
//    return new InMemoryUserDetailsManager(users);
//  }

  @Bean
  public UserDetailsManager users(DataSource dataSource) {
      JdbcUserDetailsManager users = new JdbcUserDetailsManager(dataSource);
      users.setUsersByUsernameQuery("SELECT username,password,enabled FROM users WHERE username=?");
      users.setAuthoritiesByUsernameQuery("SELECT username,role FROM users WHERE username=?");
      return users;
  }
}
