package ru.sfu.bochkarev.dcis.springbootapp.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import ru.sfu.bochkarev.dcis.springbootapp.dao.SmartphoneDAO;
import ru.sfu.bochkarev.dcis.springbootapp.models.Smartphone;


/**
 * REST контроллер, обеспечивающий доступ к данным о смартфонах.
 */
@Controller
@RequestMapping("/smartphones")
public class SmartphonesController {
  private SmartphoneDAO smartphoneDAO;

  @Autowired
  public SmartphonesController(SmartphoneDAO smartphoneDAO) {
    this.smartphoneDAO = smartphoneDAO;
  }

  //////////////////////////////////////////////////////////////
  // Черновик:
  // headers = {"Accept=application/json"}
  // produces = {"application/json"}
  // consumes = {"text/html"}
  // TODO: Узнать что и когда использовать
  //
  // https://stackoverflow.com/questions/30923249/spring-4-requestmapping-consumes-vs-headers
  // https://stackoverflow.com/questions/33009918/spring-boot-controller-content-negotiation

  /**
   * Вернуть список всех смартфонов
   */
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  public String getSmartphones(Model model) {
    // ViewResolver имеют доступ к объекту Model.
    //
    // Например, обработчик JSON представлений поместит в ответ объекты, добавленные в Model.
    // В данной функции ответ для JSON будет выглядеть: {"items": [{...}, ...]}.
    //
    // Но, как обработчик JSON представлений обработает возвращаемое значение функции
    // getSmartphones?
    model.addAttribute("items", smartphoneDAO.findAll());
    return "smartphones/items";
  }

  //////////////////////////////////////////////////////////////
  // REST
  //
  // @ResponseBody - преобразует возвращаемый объект в JSON.
  // При добавлении @ResponseBody, ответ минует ContentNegotiatingViewResolver.
  //
  // @RequestBody - преобразует тело запроса (JSON) в объект.
  //

  @GetMapping(produces = {"application/json"})
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<Smartphone> getSmartphones() {
    return smartphoneDAO.findAll();
  }

  @GetMapping(value = "/{id}", produces = {"application/json"})
  @ResponseBody
  public Smartphone getSmartphoneById(@PathVariable("id") Integer id) {
    Smartphone smartphone = smartphoneDAO.findById(id);
    if (smartphone == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return smartphone;
  }

  @PostMapping(produces = {"application/json"}, consumes = {"application/json"})
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public Smartphone creatingNewSmartphone(HttpServletResponse response,
      @Valid @RequestBody Smartphone smartphone, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    smartphoneDAO.insert(smartphone);
    response.setHeader("Location", "/smartphones/" + smartphone.getId());

    return smartphone;
  }

  @PutMapping(value = "/{id}", consumes = {"application/json"}) //
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void updateSmartphoneById(@PathVariable("id") Integer id,
      @Valid @RequestBody Smartphone smartphone, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    if (smartphoneDAO.updateById(id, smartphone) == 0)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);

  }

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteSmartphoneById(@PathVariable("id") Integer itemId) {
    Smartphone smartphone = smartphoneDAO.findById(itemId);

    if (smartphoneDAO.removeById(itemId) == 0) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

  }

  //////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////

  /**
   * Вернуть форму для создания нового смартфона.
   */
  @GetMapping(value = "/new", produces = {"text/html"})
  public String getCreateSmartphoneForm(@ModelAttribute("smartphone") Smartphone smartphone) {
    return "smartphones/newItem";
  }

  /**
   * Добавление нового смартфона.
   */
  @PostMapping(value = "/new", produces = {"text/html"})
  public String creareSmartphone(@ModelAttribute("smartphone") @Valid Smartphone smartphone,
      BindingResult bindingResult) {
    if (bindingResult.hasErrors())
      return "smartphones/newItem";

    smartphoneDAO.insert(smartphone);


    return "redirect:/smartphones";
  }

  /**
   * Вернуть форму для выбора смартфона.
   */
  @GetMapping("/selection/{action}")
  public String smartphoneSelectionById(@PathVariable String action,
      @RequestParam(value = "smartphoneId", required = false) Integer smartphoneId) {
    if (smartphoneId == null)
      return "/smartphones/choosingSmartphone";

    switch (action) {
      case "edit":
        return "redirect:/smartphones/" + smartphoneId + "/" + action;
      case "del": // TODO: Переписать.
        deleteSmartphoneById(smartphoneId);
        return "redirect:/smartphones";
    }

    throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  }

  /**
   * Вернуть форму для изменения данных смартфона.
   */
  @GetMapping("/{id}/edit")
  public String getEditSmartphoneForm(Model model, @PathVariable("id") int id) {
    Smartphone smartphone = smartphoneDAO.findById(id);
    if (smartphone == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);

    model.addAttribute("smartphone", smartphone);
    return "/smartphones/editItem";
  }

  /**
   * Изменить данные смартфона.
   */
  @PostMapping("/{id}/edit")
  public String editSmartphone(@ModelAttribute("smartphone") @Valid Smartphone smartphone,
      BindingResult bindingResult, @PathVariable("id") int id) {
    if (bindingResult.hasErrors()) {
      return "/smartphones/editItem";
    }

    if (smartphoneDAO.updateById(id, smartphone) == 0)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);

    return "redirect:/smartphones";
  }

  /**
   * Отобразить список смартфонов год выпуска которых больше определенного значения.
   */
  @GetMapping("/filter/year")
  public String getSmartphonesFilterByYear(Model model,
      @RequestParam(value = "minRreleaseYear", required = false) Integer minRreleaseYear) {

    if (minRreleaseYear == null)
      return "smartphones/filterYear";

    model.addAttribute("items", smartphoneDAO.findWhereReleaseYearMore(minRreleaseYear));
    return "smartphones/items";
  }

  /**
   * Оформление покупки смартфона
   */
  @GetMapping("/buy")
  public String buySmartphones(@RequestParam(value = "itemId") Integer itemId) {
    Smartphone smartphone = smartphoneDAO.findById(itemId);
    if (smartphone == null || !smartphone.getEnabled())
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);

    smartphone.setEnabled(false);
    smartphoneDAO.updateById(itemId, smartphone);

    return "redirect:/smartphones";
  }
}
