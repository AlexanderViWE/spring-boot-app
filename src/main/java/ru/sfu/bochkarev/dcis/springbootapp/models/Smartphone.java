package ru.sfu.bochkarev.dcis.springbootapp.models;

import java.io.Serializable;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;


/**
 * Смартфон
 */
public class Smartphone implements Serializable {
  private Integer id;
  private Boolean enabled = true;

  @NotNull
  @NotBlank
  @Size(min=1, max=32)
  private String modelType;

  @NotNull
  @NotBlank
  @Size(min=1, max=32)
  private String manufacturer;

  @NotNull
  @NotBlank
  private String processorType;

  @NotNull
  @Min(value = 0)
  private Integer weight;

  @NotNull
  @Min(value = 1000)
  private Integer releaseYear;
    
  public Smartphone() {}

  public Smartphone(Integer id, String modelType, String manufacturer, String processorType,
      Integer weight, Integer releaseYear) {
    this.setId(id);
    this.setModelType(modelType);
    this.setManufacturer(manufacturer);
    this.setProcessorType(processorType);
    this.setWeight(weight);
    this.setReleaseYear(releaseYear);
  }

  @Override
  public String toString() {
    return "Smartphone(id=" + getId() + ", modelType=" + getModelType() + ", manufacturer="
        + getManufacturer() + ", releaseYear=" + getReleaseYear() + ", processorType="
        + getProcessorType() + ", weight=" + getWeight() + ")";
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the modelType
   */
  public String getModelType() {
    return modelType;
  }

  /**
   * @param modelType the modelType to set
   */
  public void setModelType(String modelType) {
    this.modelType = modelType;
  }

  /**
   * @return the manufacturer
   */
  public String getManufacturer() {
    return manufacturer;
  }

  /**
   * @param manufacturer the manufacturer to set
   */
  public void setManufacturer(String manufacturer) {
    this.manufacturer = manufacturer;
  }

  /**
   * @return the processorType
   */
  public String getProcessorType() {
    return processorType;
  }

  /**
   * @param processorType the processorType to set
   */
  public void setProcessorType(String processorType) {
    this.processorType = processorType;
  }

  /**
   * @return the weight
   */
  public Integer getWeight() {
    return weight;
  }

  /**
   * @param weight the weight to set
   */
  public void setWeight(Integer weight) {
    this.weight = weight;
  }

  /**
   * @return the releaseYear
   */
  public Integer getReleaseYear() {
    return releaseYear;
  }

  /**
   * @param releaseYear the releaseYear to set
   */
  public void setReleaseYear(Integer releaseYear) {
    this.releaseYear = releaseYear;
  }

  /**
   * @return the enabled
   */
  public Boolean getEnabled() {
    return enabled;
  }

  /**
   * @param enabled the enabled to set
   */
  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

}
