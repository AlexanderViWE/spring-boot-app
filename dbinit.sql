DROP TABLE IF EXISTS smartphone CASCADE;
CREATE TABLE smartphone
(
	id				serial,
	manufacturer	VARCHAR(32)	NOT NULL,  -- Производитель
	modelType		VARCHAR(32)	NOT NULL,  -- Название модели
	processorType	TEXT		NOT NULL,  -- Название процессора
	weight			integer		NOT NULL,  -- Вес смартфона в граммах
	releaseYear		integer, 			   -- Год выхода модели в продажу
	enabled			boolean 	NOT NULL	DEFAULT true, -- Количество 
	PRIMARY KEY (id)
);

insert into smartphone (manufacturer, modelType, processorType, weight, releaseYear) values 
('realme', 'C25S', 'MediaTek Helio G85', 209, 2020),
('realme', 'C10', 'prosMA 1010', 209, 2016),
('realme', 'C14', 'PSW 2030', 209, 2017),
('Samsung', 'A22 Lite', 'INOI ', 180, 2021),
('Samsung', 'Galaxy S10e', 'Samsung Exynos 9820', 150, 2019),
('Samsung', 'Galaxy S5', 'Samsung Exynos 2420', 300, 2016),
('Apple', 'iPhone 13', 'Apple A15 Bionic', 173, 2019),
('Apple', 'iPhone v1', 'Apple A12 Bionic', 221, 2008);


DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE users
(
	id			serial,
	username	VARCHAR(255)	NOT NULL UNIQUE, 
	password	VARCHAR(255)	NOT NULL, 
	role		VARCHAR(255)	NOT NULL,
	enabled		boolean 		NOT NULL,

	PRIMARY KEY (id)
);

INSERT INTO users (username, password, role, enabled) VALUES 
('admin', '$2a$10$UlTfN6.3m60tKqxQC88kkuDz9xC20.uFocvwlnN93h4U28RxAKISi', 'ROLE_ADMIN', TRUE);
